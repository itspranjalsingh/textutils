from django.http import HttpResponse
from django.shortcuts import render
def index(request):
    # return HttpResponse("hello")
    params = {'name':"Pranjal",'place':'Saturn'}
    return render(request, "index.html",params)

def about(request):
    return HttpResponse("about me")

def favs(request):
    return HttpResponse('<a href="www.facebook.com">FB</a><br/><a href="www.google.com">Google</a><br/><a href="www.reddit.com">Reddit</a><br/>')

def analyze(request):
    djtext = request.POST.get('text','default')
    removepuncbox = request.POST.get('removepunc','off')
    punctuations = '''!@()[]{}-;:'"|?/#$%^&*_~,.'''
    capitalize = request.POST.get('capitalize','off')
    newlineremover = request.POST.get('newlineremover','off')
    x = ''
    if removepuncbox == 'on':
        analyzed_text = ''
        for char in djtext:
            if char not in punctuations:
                analyzed_text=analyzed_text+char
        x = x+' Removed Punctuation'
        params = {'purpose':x,'analyzed_text':analyzed_text}
    djtext  = analyzed_text
    if capitalize == 'on':
        analyzed_text=''
        for char in djtext:
            analyzed_text = analyzed_text+char.upper()
        x = x + ' Capitalize'
        params = {'purpose':x,'analyzed_text':analyzed_text}
    djtext  = analyzed_text
    if newlineremover == 'on':
        analyzed_text=''
        for char in djtext:
            if char != '\n' and char!='\r':
                analyzed_text = analyzed_text+char
        x = x+' Remove new lines'
        params = {'purpose':x,'analyzed_text':analyzed_text}
    if newlineremover == 'off' and capitalize == 'off' and removepuncbox == 'off':
        return HttpResponse("<h3>Error: Switch on one of the switch</h3>")
    return render(request,'analyze.html',params)